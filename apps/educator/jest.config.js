module.exports = {
    name: 'educator',
    preset: '../../jest.config.js',
    coverageDirectory: '../../coverage/apps/educator',
    snapshotSerializers: [
        'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
        'jest-preset-angular/build/AngularSnapshotSerializer.js',
        'jest-preset-angular/build/HTMLCommentSerializer.js'
    ]
};
